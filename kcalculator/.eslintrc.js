module.exports = {
    root: true,
    env: {
      browser: true,
      node: true
    },
    parserOptions: {
      parser: 'babel-eslint'
    },
    extends: [
      'eslint:recommended',
      // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
      // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
      'plugin:vue/recommended',
      //'plugin:prettier/recommended'
    ],
    // required to lint *.vue files
    plugins: [
      'vue'
    ],
    // add your custom rules here
    rules: {
      'semi': [2, 'never'],
      'no-console': 'off',
      'vue/max-attributes-per-line': 'off',
      'vue/no-unused-vars': 'off',
      'no-unused-vars': 'off',
      'vue/attributes-order' : 'off',
      'vue/singleline-html-element-content-newline' : 'off',
      'vue/order-in-components' : 'off',
      'no-undef' : 'off',
      'vue/require-prop-types': 'off'
      //'prettier/prettier': ['error', { 'semi': false }]
    },
    ignorePatterns: [
        'test/', '__mocks__/', 'docs/'
    ],
  }