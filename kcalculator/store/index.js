export const state = () => ({
  user: {}
})

export const mutations = {
  setUser (state, user) {
    state.auth.user = user
  }
}