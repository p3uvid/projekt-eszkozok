/** Express router providing food related routes
 * @module api/routes/food
 * @requires express
 * @requires assert
 */

/**
 * Express module
 * @const
 */
const express = require('express')

/**
 * Express router to mount food related functions on.
 * @type {object}
 * @const
 */
const router = express.Router()

/**
 * Assert module
 * @const
 */
const assert = require('assert')

/** 
 * Mongo db client module 
 * @const MongoClient 
*/
const MongoClient = require('mongodb').MongoClient

/** 
 * Mongo db url 
 * @const uri 
*/
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"

/**
 * Mongo db client  
 * @const client
*/
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

/**
 * GET ALL FOOD DATA
 * @name get/food
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 * @returns {object} All food from the database.
 */
router.get('/food', function(req, res, next) {
  client.db("kcalculator").collection("food").find({}).toArray(function(err, docs) {
    assert.equal(err, null)
    // console.log("Found the following records")
    // console.log(docs)
    res.json(docs)
  })
})

/**
 * REGISTER A NEW FOOD TO DB
 * @name post/food/register
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/food/register', function(req, res, next) {
    client.db("kcalculator").collection("food").find({ name: req.body.name })
      .toArray(function(err, docs) {
        assert.equal(err, null)
        if (docs.length && (docs[0].name == req.body.name)) {
          return res.status(200).send({ error: 'Existing food' })
        }
        client.db("kcalculator").collection("food").insertOne({
          name: req.body.name,
          kcal: req.body.kcal
        })
        return res.sendStatus(200)
    })
  })

/**
 * CONSUME FOOD
 * @name post/food/consume
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/food/consume', function(req, res, next) {
  // console.log(req.body.userId)
  client.db("kcalculator").collection("consumed_food").insertOne({
    userId: req.body.userId,
    consumedFoodId: req.body.consumedFoodId,
    day: new Date(),
    consumedGram: req.body.consumedGram
  })

  return res.sendStatus(200)
})

/**
 * DRINK WATER
 * @name post/food/water/consume
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/food/water/consume', function(req, res, next) {
  client.db("kcalculator").collection("consumed_water").find({ userId: req.body.userId })
      .toArray(function(err, docs) {
        assert.equal(err, null)

        for (let i = 0; i < docs.length; i++) {
          //found the same day
          if (new Date(docs[i].day).toLocaleDateString() == new Date(req.body.day).toLocaleDateString()) {
            client.db("kcalculator").collection("consumed_water").updateOne({
              userId: req.body.userId
            }, {
              $inc: { amount: req.body.amount }
            })
            return res.sendStatus(200)
          }
        }

        //new day
        client.db("kcalculator").collection("consumed_water").insertOne({
          userId: req.body.userId,
          day: new Date(),
          amount: req.body.amount
        })
        return res.sendStatus(200)
    })
})

/**
 * GET ALL FOOD DATA BY USER ID
 * @name get/food/byuser
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 * @returns {object} All food from the database by user id.
 */
router.get('/food/byuser', function(req, res, next) {
  // console.log(req.query)
  client.db("kcalculator").collection("consumed_food").find({ userId: req.query.userId })
  .toArray(function(err, docs) {
    assert.equal(err, null)
    res.json(docs)
  })
})

/**
 * GET ALL WATER DATA BY USER ID
 * @name get/food/water/byuser
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 * @returns {object} All water from the database by user id.
 */
router.get('/food/water/byuser', function(req, res, next) {
  // console.log(req.query)
  client.db("kcalculator").collection("consumed_water").find({ userId: req.query.userId })
  .toArray(function(err, docs) {
    assert.equal(err, null)
    res.json(docs)
  })
})

module.exports = router