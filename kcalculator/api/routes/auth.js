/** Express router providing auth related routes
 * @module api/routes/auth
 * @requires express
 * @requires assert
 */

/**
 * Express module
 * @const
 */
const express = require('express')

/**
 * Express router to mount auth related functions on.
 * @type {object}
 * @const
 */
const router = express.Router()

/**
 * Assert module
 * @const
 */
const assert = require('assert')

/** 
 * Mongo db client module 
 * @const MongoClient 
*/
const MongoClient = require('mongodb').MongoClient

/** 
 * Mongo db url 
 * @const uri 
*/
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"

/**
 * Mongo db client  
 * @const client
*/
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

var ObjectID = require('mongodb').ObjectID

/**
 * Actual user
 * @inner
 * @var user
 */
let user = {}

/**
 * LOGIN
 * @name post/auth/login
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 * @returns {(object|string)} Actual user or incorrect email address
 */
router.post('/auth/login', function(req, res, next) {
  client.db("kcalculator").collection("user").find({ email: req.body.email })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      if (!docs.length) {
        return res.status(200).send({ error: 'Incorrect email' })
      }
      user = docs[0]
      return res.status(200).send(docs[0])
  })
})

/**
 * USER PROFILE
 * @name get/auth/user
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/auth/user', function(req, res, next) {
  res.send(user)
})

/**
 * LOGOUT
 * @name post/auth/logout
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/auth/logout', function(req, res, next) {
  res.sendStatus(200)
})

/**
 * REGISTER
 * @name post/auth/register
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.post('/auth/register', function(req, res, next) {
  client.db("kcalculator").collection("user").find({ email: req.body.email })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      if (docs.length && (docs[0].email == req.body.email)) {
        return res.status(200).send({ error: 'Existing email address' })
      }
      client.db("kcalculator").collection("user").insertOne({
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        sex: req.body.sex ? req.body.sex : null, 
        height: req.body.height ? req.body.height : null,
        weight: req.body.weight ? req.body.weight : null
      })
      return res.sendStatus(200)
  })
})

/* GET USER'S DATA */
router.get('/user/:userId', function(req, res, next) {
  client.db("kcalculator").collection("user").find({ _id: ObjectID(req.params.userId) })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      res.json(docs)
  })
})

module.exports = router