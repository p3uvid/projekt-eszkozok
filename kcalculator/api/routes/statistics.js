var express = require('express')
var router = express.Router()
const assert = require('assert')
var ObjectID = require('mongodb').ObjectID

// MongoDB connection
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

/* GET LOGGED IN USER'S WEIGHTS */
router.post('/statistics/weight', function(req, res, next) {
  client.db("kcalculator").collection("weightTracker").find({ userId: req.body.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      res.json(docs)
  })
})

/* ADD NEW WEIGHT */
router.post('/statistics/addWeight', function(req, res, next) {
  let actualDate = new Date()
  let year = actualDate.getFullYear()
  let month = actualDate.getMonth() + 1
  let day = actualDate.getDate()
  let found = false
  client.db("kcalculator").collection("weightTracker").find( { userId: req.body.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      docs.forEach(w => {
        if (w.date.getFullYear() == year && w.date.getMonth() + 1 == month && w.date.getDate() == day) {
          found = true
          client.db("kcalculator").collection("weightTracker").updateOne(
            { _id: ObjectID(w._id) }, 
            { $set: {
              weight: req.body.weight,
              date: actualDate
            }
          })
        }

      })
      if (!found) {
        client.db("kcalculator").collection("weightTracker").insertOne({
          userId: req.body.userId,
          weight: req.body.weight,
          date: new Date()
        })
      }
      client.db("kcalculator").collection("user").updateOne(
        { _id: ObjectID(req.body.userId) }, 
        { $set: {
          weight: req.body.weight,
        }
      })
    })
  res.sendStatus(200)
})

module.exports = router