var express = require('express')
var router = express.Router()
const assert = require('assert')

// MongoDB connection
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

/* GET ALL SPORT DATA */
router.get('/sport', function(req, res, next) {
  client.db("kcalculator").collection("sport").find({}).toArray(function(err, docs) {
    assert.equal(err, null)
    //console.log("Found the following records")
    // console.log(docs)
    res.json(docs)
  })
})

router.get('/sport/byuser', function(req, res, next) {
  client.db("kcalculator").collection("completed_sport").find({ userId: req.query.userId })
  .toArray(function(err, docs) {
    assert.equal(err, null)
    res.json(docs)
  })
})

/* REGISTER A NEW SPORT TO DB*/
router.post('/sport/register', function(req, res, next) {
    client.db("kcalculator").collection("sport").find({ name: req.body.name })
      .toArray(function(err, docs) {
        assert.equal(err, null)
        if (docs.length && (docs[0].name == req.body.name)) {
          return res.status(200).send({ error: 'Existing sport' })
        }
        client.db("kcalculator").collection("sport").insertOne({
          name: req.body.name,
          kcalPerHour: req.body.kcalPerHour
        })
        return res.sendStatus(200)
    })
  })

/* COMPLETE SPORT */
router.post('/sport/complete', function(req, res, next) {

  client.db("kcalculator").collection("completed_sport").insertOne({
    userId: req.body.userId,
    completedSportId: req.body.completedSportId,
    day: new Date(),
    movementDuration: req.body.movementDuration
  })

  return res.sendStatus(200)
})

module.exports = router