var express = require('express')
var router = express.Router()
const assert = require('assert')
var ObjectID = require('mongodb').ObjectID

// MongoDB connection
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

/* GET ALL FORUM NOTES */
router.get('/forum', function(req, res, next) {
  client.db("kcalculator").collection("forum").find({})
    .toArray(function(err, docs) {
      assert.equal(err, null)
      res.json(docs)
  })
})

/* ADD NEW FORUM NOTE */
router.post('/forum/new', function(req, res, next) {
  client.db("kcalculator").collection("forum").insertOne({
    userId: req.body.userId,
    name: req.body.name,
    title: req.body.title,
    text: req.body.text,
    date: new Date(),
    like: 0
  })
  res.sendStatus(200)
})

/* GET LIKES OF USER */
router.get('/forum/forumLikes/:userId', function(req, res, next) {
  client.db("kcalculator").collection("forumLikes").find({ userId: req.params.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      res.json(docs)
    })
})

/* SAVE LIKES */
router.post('/forum/saveLikes', function(req, res, next) {
  let found = false
  client.db("kcalculator").collection("forumLikes").find( { userId: req.body.userId, noteId: req.body.noteId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      if (docs.length > 0) {
        found = true
        client.db("kcalculator").collection("forumLikes").deleteOne({ _id: ObjectID(docs[0]._id) })}

      if (!found) {
        client.db("kcalculator").collection("forumLikes").insertOne({
          userId: req.body.userId,
          noteId: req.body.noteId
        })
      }
    })
  
  client.db("kcalculator").collection("forum").updateOne(
    { _id: ObjectID(req.body.noteId) }, 
    { $set: {
      like: parseInt(req.body.like)
    }
  })
  res.sendStatus(200)
})

/* GET COMMENTS OF A POST */
router.get('/forum/comments/:forumId', function(req, res, next) {
  client.db("kcalculator").collection("comment").find({ forumId: req.params.forumId })
  .toArray(function(err, docs) {
    assert.equal(err, null)
    res.send(docs)
  })
})

/* ADD NEW COMMENT */
router.post('/forum/addComment', function(req, res, next) {
  client.db("kcalculator").collection("comment").insertOne({
    userId: req.body.userId,
    forumId: req.body.forumId,
    text: req.body.text,
    name: req.body.name,
    date: new Date()
  })
  res.sendStatus(200)
})

module.exports = router