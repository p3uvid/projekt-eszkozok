var express = require('express')
var router = express.Router()
const assert = require('assert')
var ObjectID = require('mongodb').ObjectID

// MongoDB connection
const MongoClient = require('mongodb').MongoClient
const uri = "mongodb+srv://admin:admin@cluster0-4lroe.azure.mongodb.net/test?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true })
client.connect()

router.post('/kcal', function(req, res, next) {
  let date = req.body.date.split('-')
  let items = []
  client.db("kcalculator").collection("consumed_food").find({ userId: req.body.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      docs.forEach(item => {
        if (item.day.getFullYear() == parseInt(date[0]) && item.day.getMonth() + 1 == parseInt(date[1]) && item.day.getDate() == parseInt(date[2])) {
          items.push(item)
        }
      })
      return res.status(200).send(items)
    })
})

router.post('/kcal/burned', function(req, res, next) {
  let date = req.body.date.split('-')
  let items = []
  client.db("kcalculator").collection("completed_sport").find({ userId: req.body.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      docs.forEach(item => {
        if (item.day.getFullYear() == parseInt(date[0]) && item.day.getMonth() + 1 == parseInt(date[1]) && item.day.getDate() == parseInt(date[2])) {
          items.push(item)
        }
      })
      return res.status(200).send(items)
    })
})

router.post('/water', function(req, res, next) {
  let date = req.body.date.split('-')
  let sum = 0
  client.db("kcalculator").collection("consumed_water").find({ userId: req.body.userId })
    .toArray(function(err, docs) {
      assert.equal(err, null)
      docs.forEach(item => {
        if (item.day.getFullYear() == parseInt(date[0]) && item.day.getMonth() + 1 == parseInt(date[1]) && item.day.getDate() == parseInt(date[2])) {
          sum += item.amount
        }
      })
      return res.status(200).send({ sum })
    })
})

module.exports = router