import Vue from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCommentDots, faHeart, faComments, faUtensils, faHamburger, faGlassWhiskey, faRunning } from '@fortawesome/free-solid-svg-icons'

library.add(faCommentDots, faHeart, faComments, faUtensils, faHamburger, faGlassWhiskey, faRunning)
Vue.component('font-awesome-icon', FontAwesomeIcon)