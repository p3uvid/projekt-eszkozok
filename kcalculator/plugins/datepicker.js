import Vue from 'vue'
import DatePicker from 'vuejs-datepicker'

Vue.use({
  install(Vue, options) {
    Vue.component('datepicker', DatePicker)
  }
})