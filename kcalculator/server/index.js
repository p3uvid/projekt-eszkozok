const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(bodyParser.json())
app.use(cors())

// Routes for authentication
const authRoutes = require('../api/routes/auth')
app.use('/api', authRoutes)

// Routes for statistics
const statRoutes = require('../api/routes/statistics')
app.use('/api', statRoutes)

//Routes for profile
const profileRoutes = require('../api/routes/profile')
app.use('/api', profileRoutes)
// Routes for food
const foodRoutes = require('../api/routes/food')
app.use('/api', foodRoutes)

// Routes for forum
const forumRoutes = require('../api/routes/forum')
app.use('/api', forumRoutes)

// Routes for sport
const sportRoutes = require('../api/routes/sport')
app.use('/api', sportRoutes)

// Routes for kcal-tracker
const kcalRoutes = require('../api/routes/kcal')
app.use('/api', kcalRoutes)

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  let { host, port } = nuxt.options.server
  
  await nuxt.ready()

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })

}
start()