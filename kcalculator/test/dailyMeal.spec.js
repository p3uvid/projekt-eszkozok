import {shallowMount} from '@vue/test-utils'
import { BForm, BFormGroup, BFormInput, BButton } from 'bootstrap-vue'
import dailyMeal from '@/components/dailyMeal.vue'
import mockAxios from 'axios'

const wrapper = shallowMount(dailyMeal, {
  stubs: {
    "b-form": BForm,
    "b-form-group": BFormGroup,
    "b-form-input": BFormInput,
    "b-button": BButton
  }
});

describe('dailyMeal', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.clearAllMocks();
  });

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('should be defined', () => {
    expect(dailyMeal).toBeDefined()
  })
  
  // test('mounted should be defined', () => {
  //   expect(wrapper.vm.mounted).toBeDefined()
  // })
  
  // test('mounted function works', async () => {
  //   mockAxios.get.mockImplementationOnce(() =>
  //     Promise.resolve({
  //       data: { foods: [{name: "food1", kcal: 152}] }
  //     })
  //   );
  //   await wrapper.vm.mounted()
  //   expect(wrapper.vm.foods).toEqual({foods: [{name: "food1", kcal: 152}]});
  //   expect(mockAxios.get).toHaveBeenCalledTimes(1);
  //   expect(mockAxios.get).toHaveBeenCalledWith(
  //     "http://localhost:3000/api/food"
  //   );
    
  // })
  
  test('registerFood should be defined', () => {
    expect(wrapper.vm.registerFood).toBeDefined()
  })
  
  test('registerFood function works (successfully registered)', async () => {
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({
        data: "data"
      })
    );
    wrapper.vm.name = "food2"
    wrapper.vm.kcal = 100
    await wrapper.vm.registerFood()
    expect(wrapper.vm.submitMessage).toEqual("Successfully submitted");
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    expect(mockAxios.post).toHaveBeenCalledWith(
      "http://localhost/api/food/register", 
      {
        name: "food2",
        kcal: 100
      }
    );
  })

  test('registerFood function works (successfully registered)', async () => {
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({
        data: "data"
      })
    );
    wrapper.vm.name = "food2"
    wrapper.vm.kcal = 100
    await wrapper.vm.registerFood()
    expect(wrapper.vm.submitMessage).toEqual("Successfully submitted");
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    expect(mockAxios.post).toHaveBeenCalledWith(
      "http://localhost/api/food/register", 
      {
        name: "food2",
        kcal: 100
      }
    );
  })
  
  test('consumeFood should be defined', () => {
    expect(wrapper.vm.consumeFood).toBeDefined()
  })
  
  test('consumeFood function works', async () => {
    wrapper.vm.$auth = require('~/middleware/auth')
    wrapper.vm.$auth.user = {
      _id: "5e638be58b92342d74d3ef18",
      name: "Test user",
      username: "testuser1234",
      email: "testuser1234@test.com",
      password: "$2a$10$6ZhiecYSWq08GnuRt5ql1.ulfiTIXvdMlQRhqYrRYPalhuHFC/KvW",
      sex: "F",
      height: "170",
      weight: "70"
    }
    wrapper.vm.consumed = "Alma"
    wrapper.vm.foods = [{ _id: "5e676ee9c0b9a62b6cbfd05a", name: "Alma", kcal: 150 }]
    wrapper.vm.consumedGram = 100
    await wrapper.vm.consumeFood()
    expect(wrapper.vm.consumedSubmitMessage).toEqual("Eated!");
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    /*expect(mockAxios.post).toHaveBeenCalledWith(
      "http://localhost/api/food/consume", 
      {
        userId: "5e638be58b92342d74d3ef18",
        consumedFoodId: "5e676ee9c0b9a62b6cbfd05a",
        day: new Date(),
        consumedGram: 100
      }
    );*/
  })

  test('consumeFood function works with error (no food id)', async () => {
    await wrapper.vm.consumeFood()
    expect(wrapper.vm.consumedErrorMessage).toEqual("Food not found!");
  })
});
