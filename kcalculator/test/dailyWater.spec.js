import {shallowMount} from '@vue/test-utils'
import { BButton } from 'bootstrap-vue'
import dailyWater from '@/components/dailyWater.vue'
import mockAxios from 'axios'

const wrapper = shallowMount(dailyWater, {
  stubs: {
    "b-button": BButton
  }
});

describe('dailyWater', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.clearAllMocks();
  });

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('addWater should be defined', () => {
    expect(wrapper.vm.addWater).toBeDefined()
  })

  test('addWater method', () => {
    wrapper.vm.addWater()
    expect(wrapper.vm.water).toBe(1)
    wrapper.vm.addWater()
    expect(wrapper.vm.water).toBe(2)
  })

  test('remWater should be defined', () => {
    expect(wrapper.vm.remWater).toBeDefined()
  })

  test('remWater method', () => {
    wrapper.vm.remWater()
    expect(wrapper.vm.water).toBe(1)
  })

  test('drink should be defined', () => {
    expect(wrapper.vm.drink).toBeDefined()
  })
  
  test('drink function works', async () => {
    wrapper.vm.$auth = require('~/middleware/auth')
    wrapper.vm.$auth.user = {
      _id: "5e638be58b92342d74d3ef18",
      name: "Test user",
      username: "testuser1234",
      email: "testuser1234@test.com",
      password: "$2a$10$6ZhiecYSWq08GnuRt5ql1.ulfiTIXvdMlQRhqYrRYPalhuHFC/KvW",
      sex: "F",
      height: "170",
      weight: "70"
    }
    await wrapper.vm.drink()
    expect(wrapper.vm.submitMessage).toEqual("Consumed!")
    expect(mockAxios.post).toHaveBeenCalledTimes(1);
    /*expect(mockAxios.post).toHaveBeenCalledWith(
      "http://localhost/api/food/water/consume",
      {
        userId: "5e638be58b92342d74d3ef18",
        day: new Date(),
        amount: 1
      }
    );*/
    
  })
});
