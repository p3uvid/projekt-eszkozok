import {shallowMount} from '@vue/test-utils'
import { BNavbar, BNavbarBrand, BNavbarToggle, BCollapse, BNavbarNav, BNavItem, BNavItemDropdown, BDropdownItem } from 'bootstrap-vue'
import Navbar from '@/components/headerNavbar.vue'

const $auth = {
  user: null,
  loggedIn: false,
  login() { this.loggedIn = true },
  logout() { this.user = null, this.loggedIn = false }
}

const wrapper = shallowMount(Navbar, {
  stubs: {
    "b-navbar": BNavbar,
    "b-navbar-brand": BNavbarBrand,
    "b-navbar-toggle": BNavbarToggle,
    "b-collapse": BCollapse,
    "b-navbar-nav": BNavbarNav,
    "b-nav-item": BNavItem,
    "b-nav-item-dropdown": BNavItemDropdown,
    "b-dropdown-item": BDropdownItem
  },
  mocks: {
    $auth
  }
});

describe('headerNavbar', () => {
  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  // test('logout', async () => {
  //   wrapper.vm.$auth.user = true
  //   await wrapper.vm.logout()
  //   expect(wrapper.vm.$auth.user).toBeFalsy()
  //   expect(wrapper.vm.$auth.loggedIn).toBeFalsy()
  // })
})