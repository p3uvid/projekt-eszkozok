const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname
const host = (process.env.NODE_ENV !== 'production' ? "http://localhost:3000" : "https://kcalculator-projekt.herokuapp.com" )

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/vue-apexchart.js', ssr: false },
    { src: '~plugins/datepicker.js', ssr: false },
    { src: '~/plugins/font-awesome' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: `${host}/api/auth/login`, method: 'post', propertyName: '' },
          logout: { url: `${host}/api/auth/logout`, method: 'post' },
          user: { url: `${host}/api/auth/user`, method: 'get', propertyName: false },
        }
      }
    }
  },
  /*
  ** Build configuration
  */
 build: {
  /*
   ** You can extend webpack config here
  */
  extend(config, ctx) {
     // Run ESLint on save
     if (ctx.isDev && ctx.isClient) {
       config.module.rules.push({
         enforce: "pre",
         test: /\.(js|vue)$/,
         loader: "eslint-loader",
         exclude: /(node_modules)/
       })
     }
   }
 },
  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: '../public'
  },

  router: {
    middleware: ['auth'],
    base
    //base: '' //this is whatever the project is named
    //base: '/projekt-eszkozok/' //this is whatever the project is named
  }
}
